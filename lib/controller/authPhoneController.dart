

import 'package:animationapp/constant/firebase.dart';
import 'package:animationapp/pages/otpPage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

var verificationId1;

Future<void> verifyPhoneNumber(String phone)async{
  print('the number is ${phone}');

  await auth.verifyPhoneNumber(
    phoneNumber: phone,
    verificationCompleted: (PhoneAuthCredential credential){
        // auth.signInWithPhoneNumber(credential.toString()).then((value) => 
        // print('you are sign in with number')
        // );
    },
   verificationFailed: (FirebaseException e){
     if(e.code=='invalid-phone-number'){
       print('The provided phone number is not valid');
     }
   },
    codeSent: (String verificationId,int? resendToken)async{

      verificationId1=await verificationId;
      if(verificationId1!=null){
        print('verificationid');
        print(verificationId);
              Get.to(OtpPage());
      }
    
      
    },
    timeout: Duration(seconds: 120),
   codeAutoRetrievalTimeout:(String verificationId){

   }
   );
  
}
