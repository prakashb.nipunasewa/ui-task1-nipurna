import 'package:animationapp/constant/firebase.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../controller/authPhoneController.dart';

class OtpPage extends StatefulWidget {
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  TextEditingController otpNumber = TextEditingController();

  signInWithPhone(PhoneAuthCredential phoneAuthCredential) async {
    try {
      final authCredentail =
          await auth.signInWithCredential(phoneAuthCredential);
          if(authCredentail.user!=null){
            Navigator.pushNamed(context, '/firstpage');
          }
    } on FirebaseAuthException catch (e) {

      throw e;

    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 80, left: 20),
        child: Column(
          children: [
            Form(
              child: TextFormField(
                controller: otpNumber,
                decoration: InputDecoration(labelText: 'Enter OTP'),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: ElevatedButton(
                  onPressed: () async{
                    PhoneAuthCredential phoneAuthCredential =
                      await  PhoneAuthProvider.credential(
                            verificationId: verificationId1,
                            smsCode: otpNumber.text);

                    await signInWithPhone(phoneAuthCredential);
                  },
                  child: Text('Submit')),
            ),
          ],
        ),
      ),
    );
  }
}
