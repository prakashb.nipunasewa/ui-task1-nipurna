import 'package:animationapp/controller/authPhoneController.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey=GlobalKey<FormState>();

  TextEditingController phoneNumber=TextEditingController();

  void onSubmit(){
    if(formKey.currentState!.validate()){
      setState(() {
         print('ok valid');
      });
      verifyPhoneNumber(phoneNumber.text);
      
     
    }else{
      
    }
  }
  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;
    final deviceWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: deviceHeight * 0.35,
                  width: deviceWidth,
                  child: Image.asset('assets/images/reward.png',
                  height: deviceHeight,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      LoginFormWidget(
                        deviceHeight: deviceHeight,
                        deviceWidth: deviceWidth,
                        formkey: formKey,
                        labeltext: 'Mobile Number',
                        phoneNumber:phoneNumber,
                        iconbutton: Icon(
                          Icons.phone_android,
                          color: Colors.green,
                          
                        ),
                      ),
                   
                      SizedBox(height: 20,),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Text(
                          'Forgot Password ?',
                          style: TextStyle(
                              color: Colors.green, fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(height: 20,),
                    GestureDetector(
                      onTap: onSubmit,
                      child: Container(
                        height: deviceHeight*0.08,
                        width: deviceWidth,
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(8)
                        ),
                        child: Center(
                          child: Text('Login',
                          style: TextStyle(
                            color: Colors.white
                          ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                     padding: EdgeInsets.only(left: 60),
                      child: Row(
                        children:[
                               Text('Don\'t have an account yet?'),
                               SizedBox(width: 15,),
                               GestureDetector(
                                 onTap: (){
                                   Navigator.pushNamed(context, '/registerpage');
                                 },
                                 child: Text('Register',
                                 style: TextStyle(
                                   color: Colors.green,
                                   fontWeight: FontWeight.bold
                                 ),
                                 ),
                               )
                        ]
                                       
                      ),
                    )
                  
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LoginFormWidget extends StatelessWidget {
  const LoginFormWidget(
      {required this.deviceHeight,
      required this.deviceWidth,
      required this.iconbutton,
      required this.formkey,
      required this.phoneNumber,
      required this.labeltext});

  final double deviceHeight;
  final double deviceWidth;
  final String labeltext;
  final Icon iconbutton;
  final Key formkey;
  final TextEditingController phoneNumber;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      child: Container(
        height: deviceHeight * 0.10,
      
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
           boxShadow: [
          BoxShadow(color: Colors.black12, spreadRadius: 0.4, blurRadius: 2),
        ]),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              iconbutton,
              SizedBox(width: 10),
              Expanded(
                child: Form(
                  key: formkey,
                  child: TextFormField(
                    controller: phoneNumber,
                    style: TextStyle(color: Colors.green),
                    decoration: InputDecoration(
                        labelText: labeltext,
                        labelStyle: TextStyle(
                            color: Colors.black38, fontWeight: FontWeight.bold),
                        border: InputBorder.none),
                        validator: (value){
                            if(value!.isEmpty){
                                return 'Enter phone number';
                            }
                            return null;
                       
                        },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
