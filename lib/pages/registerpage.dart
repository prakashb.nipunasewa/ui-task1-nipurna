import 'dart:async';

import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final formKey=GlobalKey<FormState>();

  TextEditingController controllerName=TextEditingController();
   TextEditingController controllerEmail=TextEditingController();
    TextEditingController controllerMobile=TextEditingController();
     TextEditingController controllerPassword=TextEditingController();
      TextEditingController controllerConfirmPassword=TextEditingController();



 

  void onSubmit(){
    if(formKey.currentState!.validate()){
      setState(() {
         print('ok valid');
      });
     
    }else{
      print('not valid');
    }
  }
  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;
    final deviceWidth = MediaQuery.of(context).size.width;
     print('the password is');
      print(controllerPassword.text);
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: [
                    
                     Container(
                  color: Colors.red,
                  height: deviceHeight * 0.20,
                  width: deviceWidth,
                  child: Image.asset('assets/images/reward.png',
                 fit: BoxFit.fill,
                  ),
                ),
                IconButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                   icon: Icon(Icons.arrow_back,
                   color: Colors.green,
                   size: 20,
                   ),
                   ),
                  ],
                ),
               
                SizedBox(
                  height: 0,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  color: Colors.white,
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        LoginFormWidget(
                          controller:controllerName,
                          deviceHeight: deviceHeight,
                          deviceWidth: deviceWidth,
                         passwordValue: controllerPassword.text,
                         // formkey: formKey,
                          labeltext: 'Name',
                          iconbutton: Icon(
                            Icons.person,
                            color: Colors.green,
                            
                          ),
                        ),
                         LoginFormWidget(
                           controller:controllerEmail,
                            passwordValue: controllerPassword.text,
                          deviceHeight: deviceHeight,
                          deviceWidth: deviceWidth,
                        //  formkey: formKey,
                          labeltext: 'Email',
                          iconbutton: Icon(
                            Icons.email,
                            color: Colors.green,
                            
                          ),
                        ),
                         LoginFormWidget(
                            passwordValue: controllerPassword.text,
                           controller:controllerMobile,
                          deviceHeight: deviceHeight,
                          deviceWidth: deviceWidth,
                          //formkey: formKey,
                          labeltext: 'Mobile Number',
                          iconbutton: Icon(
                            Icons.phone_android,
                            color: Colors.green,
                            
                          ),
                        ),
                         LoginFormWidget(
                            passwordValue: controllerPassword.text,
                           controller:controllerPassword,
                          deviceHeight: deviceHeight,
                          deviceWidth: deviceWidth,
                         // formkey: formKey,
                          labeltext: 'Password',
                          iconbutton: Icon(
                            Icons.lock,
                            color: Colors.green,
                            
                          ),
                        ),
                         LoginFormWidget(
                            passwordValue: controllerPassword.text,
                           controller:controllerConfirmPassword,
                          deviceHeight: deviceHeight,
                          deviceWidth: deviceWidth,
                         // formkey: formKey,
                          labeltext: 'Confirm Password',
                          iconbutton: Icon(
                            Icons.lock,
                            color: Colors.green,
                            
                          ),
                        ),
                     
                        SizedBox(height: 20,),
                       
                      GestureDetector(
                        onTap: onSubmit,
                        child: Container(
                          height: deviceHeight*0.08,
                          width: deviceWidth,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(8)
                          ),
                          child: Center(
                            child: Text('Register',
                            style: TextStyle(
                              color: Colors.white
                            ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                       padding: EdgeInsets.only(left: 60),
                        child: Row(
                          children:[
                                 Text('Already have an account?'),
                                 SizedBox(width: 15,),
                                 Text('Login',
                                 style: TextStyle(
                                   color: Colors.green,
                                   fontWeight: FontWeight.bold
                                 ),
                                 )
                          ]
                                         
                        ),
                      )
                    
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LoginFormWidget extends StatelessWidget {
  const LoginFormWidget(
      {required this.deviceHeight,
      required this.deviceWidth,
      required this.iconbutton,
      required this.controller,
      required this.passwordValue,
      required this.labeltext});

  final double deviceHeight;
  final double deviceWidth;
  final String labeltext;
  final Icon iconbutton;
  final String passwordValue;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      child: Container(
        height: deviceHeight * 0.10,
      
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
           boxShadow: [
          // BoxShadow(
          //   color: Colors.black12,
          //    spreadRadius: 0.4, blurRadius: 2),
        ]),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              iconbutton,
              SizedBox(width: 10),
              Expanded(
                child: TextFormField(
                  controller: controller,
                  style: TextStyle(color: Colors.green),
                  decoration: InputDecoration(
                      labelText: labeltext,
                      labelStyle: TextStyle(
                          color: Colors.black38, fontWeight: FontWeight.bold),
                      border: InputBorder.none),
                      validator: (value){
                        var password;
                        if(labeltext=='Name'){
                          if(value!.isEmpty||value.length<5){
                            return "enter name";
                          }
                          return null;
                        }
                         if(labeltext=='Email'){
                          if(value!.isEmpty||!value.contains('@')){
                            return "enter email";
                          }
                          return null;
                        }
                         if(labeltext=='Mobile Number'){
                          if(value!.isEmpty||!(value.length==10)){
                            return "Enter valid number";
                          }
                          return null;
                        }
                         if(labeltext=='Password'){
                          if(value!.isEmpty||!(value.length>6)){
                            return "enter password of more length";
                          }
                        
                          return null;
                        }
                         if(labeltext=='Confirm Password'){
                            print('password from conform password');
                           print(passwordValue);
                            
                          if(value!.isEmpty||!(value==passwordValue)){
                            return "Password does not match";
                          }
                          return null;
                        }
                         
                     
                      },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
