import 'package:animationapp/constant/firebase.dart';
import 'package:animationapp/pages/firstpage.dart';
import 'package:animationapp/pages/loginpage.dart';

import 'package:animationapp/pages/otpPage.dart';
import 'package:animationapp/pages/registerpage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await initialization;
  runApp(GetMaterialApp(
    home: HomePage(),
    theme: ThemeData(primaryColor: Colors.white),
    routes: {
      '/registerpage':(BuildContext context)=>RegisterPage(),
      '/otppage':(BuildContext context)=>OtpPage(),
       '/firstpage':(BuildContext context)=>FirstPage(),
    },
  ));
}

class HomePage extends StatelessWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:LoginPage(),
      
    );
  }
}
